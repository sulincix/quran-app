gtk4: clean
	meson build -Dgtk4=true
	ninja -C build

gtk3: clean
	meson build -Dgtk4=false
	ninja -C build

clean:
	rm -rf build
