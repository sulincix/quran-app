private bool view_orig = true;
private bool view_lang = true;

public Gtk.Widget get_ayah_view(string name,string lang){
    var box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
    box.set_css_name("ayah");
    string[] aa = get_ayah_list(name);
    string[] at = get_ayah_list_translated(name,lang);
    for(int i=0;i < aa.length; i++){
        var row = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 4);
        row.set_homogeneous(true);

        if(view_orig){
            var ll = new Gtk.Label(aa[i][i.to_string().length+1:]);
            ll.set_line_wrap(true);
            row.pack_start(ll,true,true,0);
            ll.set_xalign(1);
            ll.set_yalign(0);
        }
        if(view_lang){
            var lt = new Gtk.Label(at[i][i.to_string().length+1:]);
            lt.set_line_wrap(true);
            row.pack_start(lt,true,true,0);
            lt.set_xalign(0);
            lt.set_yalign(0);
        }

        box.pack_start(row,false,false,2);
        box.pack_start(new Gtk.Separator (Gtk.Orientation.HORIZONTAL),false,false,19);
    }
    return box;
}
