using GLib;
using Sqlite;
public Database db;

private string[] db_tmp;
private int db_callback (int n_columns, string[] values, string[] column_names){
    for (int i = 0; i < n_columns; i++) {
        db_tmp += values[i];
        
    }
    return 0;
}
    
public string[] get_ayah_list(string name){
    db_tmp = {};
    int rc = Database.open ("database/quran.db", out db);
    rc = db.exec ("select ayah from "+name, db_callback, null);
    return db_tmp;
}

public string[] get_ayah_list_translated(string name,string lang){
    db_tmp = {};
    int rc = Database.open ("database/quran_"+lang+".db", out db);
    rc = db.exec ("select ayah from "+name, db_callback, null);
    return db_tmp;
}
