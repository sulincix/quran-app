public int main(string[] args){
    Gtk.init(ref args);
    var w = new Gtk.Window();
    var view = get_ayah_view("Al_Baqara","turkish");
    
    Gtk.ScrolledWindow scrolled = new Gtk.ScrolledWindow (null, null);
    scrolled.add(view);
    w.add(scrolled);
    w.destroy.connect (Gtk.main_quit);
    w.set_default_size (800, 600);
    apply_css();
    w.show_all();
    Gtk.main();
    return 0;
}
