public void apply_css(){
    Gtk.CssProvider css_provider = new Gtk.CssProvider ();
    try{
        string data = "
        * {
            font-family: monospace;
        }
        ayah{
            font-size: 19px;
        }
        ";
        #if GTK4
        css_provider.load_from_data(data.data);
        #else
        css_provider.load_from_data(data);
        #endif
    } catch {}
    #if GTK4
    Gtk.StyleContext.add_provider_for_display (
        Gdk.Display.get_default (),
        css_provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
    #else
    Gtk.StyleContext.add_provider_for_screen (
        Gdk.Screen.get_default (),
        css_provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    );
    #endif
}

