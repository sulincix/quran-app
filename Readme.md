# Quran app Gtk
Quran app written vala and gtk3

Database files fetch from: https://github.com/Muslim-Programmers/QuranApp-Linux/tree/main/database

# Building
```shell
meson setup build
ninja -C build
```
# Dependencies
* Gtk3
* Valac
* sqlite3 (for database)
